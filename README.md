## お知らせ

2022年2月4日、Platycaのすべての掲示板提供者とオープンレジストラーを停止しました。


## 誤字について

PlatycaとPlaticaが統一されていません。どちらかが誤字です。いまさら知ったことではありません。


# 分散掲示板ネットワークPlatica

Platicaは分散掲示板ネットワークである。ネットワークの参加者を**ピア**という。ネットワークには多様な機能を持つピアが参加しており、権限が分散されている。

以下の3種類のピアがある。

* 投稿者
* 掲示板提供者
* オープンレジスタラー

## 投稿者

投稿者としてネットワークに参加するには、静的ウェブサイトを運用していればよい。あなたの静的ウェブサイトのURLが https://platyca.gitlab.io/platyca/mary/ だとすると、これがあなたの**ピアURL**になる。ピアURLの末尾に `peers.csv` を追加したURL、この例では https://platyca.gitlab.io/platyca/mary/peers.csv に、**ピアプロファイル**というファイルを置く必要がある。ピアプロファイルの例は以下である。

```
"peer","https://platyca.gitlab.io/platyca/thelema/"
"peer","https://platyca.gitlab.io/platyca/platyca/"
"post","https://platyca.gitlab.io/platyca/mary/","https://platyca.tk/boards/messages/","Hi, I'm Mary."
```

`"peer"` で始まる行は、あなたがこのネットワークに招待する参加者のピアURLである。すでにネットワークに参加していれば、誰でも新しい参加者を招待することができる。

`"post"` で始まる行は、この例では、`https://platyca.gitlab.io/platyca/mary/` が自分自身のピアURL、`https://platyca.tk/boards/messages/` が掲示板のURL、`Hi, I'm Mary.` が投稿されるメッセージである。

peer行とentry行は、それぞれ複数を記述することができる。

## 掲示板提供者

掲示板提供者は、掲示板を開設し、そこに投稿されたメッセージを表示する。https://platyca.tk/ は掲示板提供者で、https://platyca.tk/boards/messages/ という掲示板を開設している。platica.tkのピアプロファイル、すなわち https://platyca.tk/peers.csv は以下のようになっている。

```
"peer":"https://platyca.tk/"
"peer":"https://register.platyca.tk/"
"peer":"https://platyca.gitlab.io/platyca/mary/"
"board":"https://platyca.tk/boards/messages/"
"board":"https://platyca.tk/boards/sex/"
```

peer行の機能は、あらゆる種類のピアに共通である。

board行は、開設している掲示板のURLを示している。peer行とboard行は、それぞれ複数を記述することができる。

## オープンレジスタラー

Platicaのネットワークに参加するには、すでにネットワークに参加しているピアに、新たな参加者として認めてもらう必要がある。すなわち、ピアプロファイルのpeer行を追加してもらう必要がある。

これを自動的に行うのがオープンレジスタラーである。https://register.platyca.tk/ はオープンレジスタラーである。
